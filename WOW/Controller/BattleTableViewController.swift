//
//  ProfileTableViewController.swift
//  WOW
//
//  Created by SiuEdwin on 17/12/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import LNRSimpleNotifications

class BattleTableViewController: UITableViewController, UIGestureRecognizerDelegate {
  private let battleNavigationBarHeight = UIDevice.current.userInterfaceIdiom == .pad ? CGFloat(100.0):CGFloat(60.0)
  private let battleNavigationBar = BattleNavigationBar()
  private var winner : Int = Winner.not_assigned.rawValue {
    willSet(newTotalSteps) {
      if winner != Winner.not_assigned.rawValue && winner != newTotalSteps && newTotalSteps != Winner.draw.rawValue{
        let notificationManager = LNRNotificationManager()
        notificationManager.notificationsPosition = LNRNotificationPosition.top
        notificationManager.notificationsBackgroundColor = UIColor.white
        notificationManager.notificationsTitleTextColor = UIColor.black
        notificationManager.notificationsBodyTextColor = UIColor.darkGray
        notificationManager.notificationsSeperatorColor = UIColor.gray
        print("About to set totalSteps to \(newTotalSteps)")
        if newTotalSteps == 1 {
          notificationManager.showNotification(title: "New lead", body: "The current winner is \(self.blueUserName). with win record(s) \(self.blueCount)" , onTap: { () -> Void in
            let _ = notificationManager.dismissActiveNotification(completion: nil)
          })
        }else{
          notificationManager.showNotification(title: "New lead", body: "The current winner is \(self.redUserName). with win record(s) \(self.redCount)" , onTap: { () -> Void in
            let _ = notificationManager.dismissActiveNotification(completion: nil)
          })
        }
      }
    }
    didSet {
    }
  }
  private var contents = NSArray()
  private var redUserName : String = ""
  private var blueUserName : String = ""
  private var redCount : Int = 0
  private var blueCount : Int = 0
  private var Manager: Alamofire.SessionManager = {
    // Create the server trust policies
    let serverTrustPolicies: [String: ServerTrustPolicy] = [
      "test.wowjust.watch": .disableEvaluation
    ]
    // Create custom manager
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
    let manager = Alamofire.SessionManager(
      configuration: URLSessionConfiguration.default,
      serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
    return manager
  }()
  
  override init(style:UITableViewStyle ){
    super.init(nibName: nil , bundle: nil)
    self.getData()
    self.tableView!.addBackgroundViewWithLoading()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    battleNavigationBar.removeFromSuperview()
    self.navigationController!.isToolbarHidden = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    refreshControl = UIRefreshControl()
    refreshControl!.addTarget(self, action: #selector(getData), for: UIControlEvents.valueChanged)
    tableView!.addSubview(refreshControl!)
    self.navigationItem.title = "JUDGING"
    self.tableView!.contentInset = UIEdgeInsetsMake(battleNavigationBarHeight, 0.0, 0.0, 0.0)
    // whenever winner value changes, it will pop up to notify examiner
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    // tool bar
    self.navigationController!.isToolbarHidden = false
    let drawButton = UIBarButtonItem(title: "DRAW", style: .plain , target: self, action: #selector(judge))
    drawButton.tintColor = UIColor.lightGray
    let redWinButton = UIBarButtonItem(title: "RED WIN", style: .plain , target: self, action: #selector(judge))
    redWinButton.tintColor = COLOR_RED
    let blueWinButton = UIBarButtonItem(title: "BLUE WIN", style: .plain , target: self, action: #selector(judge))
    blueWinButton.tintColor = COLOR_BLUE
    self.setToolbarItems([
      redWinButton,
      UIBarButtonItem(barButtonSystemItem: .flexibleSpace , target: self, action: nil),
      drawButton,
      UIBarButtonItem(barButtonSystemItem: .flexibleSpace , target: self, action: nil),
      blueWinButton
      ], animated: true)
    
    self.tableView.separatorStyle = .none
    
    // nav bar
    self.navigationController!.navigationBar.addSubview(battleNavigationBar)
    battleNavigationBar.barStyle = .default
    battleNavigationBar.frame = CGRect(x: CGFloat(0.0), y: NAV_HEIGHT, width: DEVICE_WIDTH, height: CGFloat(battleNavigationBarHeight))
    battleNavigationBar.addTopLine()
  }
  
  func getData(){
    battleNavigationBar.alpha = 0
    Manager.request("https://test.wowjust.watch/test.json", method: .get, parameters: nil, encoding: JSONEncoding.default)
      .responseJSON { response in
        if response.result.isSuccess {
          self.battleNavigationBar.alpha = 1
          let result = response.result.value as! NSDictionary
          self.contents = result["people"] as! NSArray
          if self.contents.count == 0 {
            self.tableView.addBackgroundViewWithLabel(text: "There will be new battle coming soon")
            return
          }
          let redUser = self.contents[0] as! NSDictionary
          self.redUserName = redUser["name"] as? String ?? ""
          self.redCount = redUser["winCount"] as? Int ?? 0
          let redProfileImage = redUser["profilePicture"] as? String ?? ""
          self.battleNavigationBar.redLabel.text = self.redUserName+"("+"\(self.redCount)"+")"
          self.battleNavigationBar.redAvatar.sd_setImage(with: NSURL.init(string: redProfileImage) as URL!,placeholderImage: UIImage.init(named: "user.png"))
          let blueUser = self.contents[1] as! NSDictionary
          self.blueUserName = blueUser["name"] as? String ?? ""
          self.blueCount = blueUser["winCount"] as? Int ?? 0
          self.battleNavigationBar.blueLabel.text = self.blueUserName+"("+"\(self.blueCount)"+")"
          let blueProfileImage = blueUser["profilePicture"] as? String ?? ""
          self.battleNavigationBar.blueAvatar.sd_setImage(with: NSURL.init(string: blueProfileImage) as URL!,placeholderImage: UIImage.init(named: "user.png"))
          self.battleNavigationBar.progressView.progress = Float(Float(self.redCount)/(Float(self.redCount)+Float(self.blueCount)))
          
          if self.redCount > self.blueCount{
            self.winner = Winner.red.rawValue
          } else if self.redCount == self.blueCount {
            self.winner = Winner.draw.rawValue
          } else {
            self.winner = Winner.blue.rawValue
          }
          if self.blueCount == 0 && self.redCount == 0 {
            self.battleNavigationBar.progressView.progress = 0.5
          } else {
            self.battleNavigationBar.progressView.progress = Float(Float(self.redCount)/(Float(self.redCount)+Float(self.blueCount)))
          }
          self.tableView!.backgroundView = nil
        }else{
          self.contents = NSArray()
          self.tableView.addBackgroundViewWithLabel(text: "Network fails to connect.")
        }
        self.tableView.reloadData()
        self.refreshControl!.endRefreshing()
    }
  }
  
  func judge(){
    // post to server
  }

  override func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.contents.count
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let content = self.contents[indexPath.row] as? NSDictionary {
      let mutableContent = NSMutableDictionary(dictionary: content)
      if indexPath.row == 0 {
        mutableContent.setValue(blueUserName, forKey: "opponent")
        mutableContent.setValue(self.battleNavigationBar.progressView.progress, forKey: "winRate")
      } else {
        mutableContent.setValue(redUserName, forKey: "opponent")
        mutableContent.setValue(1-self.battleNavigationBar.progressView.progress, forKey: "winRate")
      }
      let profileTableViewController = ProfileTableViewController.init(style: .plain,value:mutableContent)
      if UIDevice.current.userInterfaceIdiom == .pad {
        profileTableViewController.modalPresentationStyle = .formSheet
        profileTableViewController.modalTransitionStyle = .coverVertical
        profileTableViewController.preferredContentSize = CGSize(width: DEVICE_WIDTH-180, height: DEVICE_HEIGHT-180)
        self.present(profileTableViewController, animated: true, completion: nil)
      } else {
        self.navigationController!.pushViewController(profileTableViewController, animated: true)
      }
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return (DEVICE_HEIGHT-NAV_HEIGHT-battleNavigationBarHeight-STATUS_BAR_HEIGHT-self.navigationController!.toolbar.frame.size.height)/2
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell :BattleTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "BattleTableViewCell") as? BattleTableViewCell
    if cell == nil {
      cell = BattleTableViewCell.init(style: .default, reuseIdentifier: "BattleTableViewCell")
    }
    if let content = self.contents[indexPath.row] as? NSDictionary {
      let imagesURL = content["images"] as? NSArray ?? NSArray()
      if imagesURL.count > 0 {
        let imageURL = imagesURL[0] as? String ?? ""
        cell.backgroundImage?.sd_setImage(with: NSURL.init(string: imageURL) as URL!)
      }
      
      var titleLabelText = content["name"] as? String ?? ""
      let backgroundColor : UIColor!
      if indexPath.row == 0 {
        backgroundColor = COLOR_RED_LOW_ALPHA
      } else {
        backgroundColor = COLOR_BLUE_LOW_ALPHA
      }
      
      if winner == indexPath.row {
        cell.trophy.isHidden = false
        titleLabelText = " " + titleLabelText + "      " // just want to include the highlight with trophy
      } else {
        cell.trophy.isHidden = true
        titleLabelText = " " + titleLabelText + " "
      }
      cell.updateTitleLabelWithAttributedText(text: NSAttributedString.init(string:titleLabelText, attributes: [NSBackgroundColorAttributeName:backgroundColor]))
    }
    cell.selectionStyle = .none
    return cell
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    if self.view.window == nil {
      self.view = nil
    }
  }
}
