//
//  ProfileTableViewController.swift
//  WOW
//
//  Created by SiuEdwin on 17/12/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController, UIGestureRecognizerDelegate {
  let headerViewY = UIDevice.current.userInterfaceIdiom == .pad ? 0 : STATUS_BAR_HEIGHT + NAV_HEIGHT
  let headerView = UIView.init()
  var value : NSDictionary!
  var content = NSArray()
  private var tapOutsideRecognizer: UITapGestureRecognizer!
  init(style:UITableViewStyle ,value:NSDictionary){
    super.init(nibName: nil , bundle: nil)
    self.value = value
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.navigationItem.title = value["name"] as? String ?? ""
    let rankHeight = CGFloat(30)
    headerView.frame = CGRect(x: 0, y: headerViewY, width: self.view.frame.width, height: self.view.frame.width * 1/2 + rankHeight)
    let headerContentView = ProfileHeaderView()
    headerContentView.avatar.sd_setImage(with: NSURL.init(string: value["profilePicture"] as? String ?? "") as URL!,placeholderImage: UIImage.init(named: "user.png"))
    let rank = value["ranking"] as? Int ?? -1
    if rank >= 0 {
      headerContentView.titleLabel.text = "Rank: " + String(rank)
    }
    headerView.addSubview(headerContentView)
    headerContentView.translatesAutoresizingMaskIntoConstraints = false
    headerContentView.addAutolayoutConstraintsSameFrameWithSuperView()
    let imagesURL = value["images"] as? NSArray ?? NSArray()
    if imagesURL.count > 0 {
      let imageURL = imagesURL[1] as? String ?? ""
      headerContentView.backgroundImage.sd_setImage(with: NSURL.init(string: imageURL) as URL!)
    }
    
    self.tableView.tableHeaderView = headerView
    self.tableView.separatorStyle = .none
  }
  
  override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let offsetY = scrollView.contentOffset.y;
    if headerView.subviews.count > 0 {
      let headerContentView = headerView.subviews[0]
      headerContentView.transform = CGAffineTransform(translationX: 0, y: min(0, offsetY+headerViewY));
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return self.tableView.frame.width/2 + 50
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }
  
  override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let view = UIView()
    let blurView : UIVisualEffectView! = UIVisualEffectView(effect: UIBlurEffect(style: .light))
    view.addSubview(blurView)
    blurView.addAutolayoutConstraintsSameFrameWithSuperView()
    
    let titleLabel = UILabel()
    titleLabel.font = UIFont(name: FONT, size: 18)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .black
    view.addSubview(titleLabel)
    titleLabel.addAutolayoutConstraints("V:|-10-[view]-10-|",horizontal: "H:|-15-[view]-15-|")
    let userName = value["name"] as? String ?? ""
    titleLabel.text = userName + "'s Battles"
    view.addBotLine()
    return view
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return 1
  }
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell : ProfileTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell!
    if cell == nil {
      cell = ProfileTableViewCell.init(style: .subtitle, reuseIdentifier: "ProfileTableViewCell")
    }
    let userName = value["name"] as? String ?? ""
    let opponentName = value["opponent"] as? String ?? ""
    cell.titleLabel.text = userName + "  VS  " + opponentName
    cell.selectionStyle = .none
    let imagesURL = value["images"] as? NSArray ?? NSArray()
    if imagesURL.count > 0 {
      let imageURL = imagesURL[0] as? String ?? ""
      cell.backgroundImage.sd_setImage(with: NSURL.init(string: imageURL) as URL!)
    }
    cell.updateTitleLabelWithText(text: userName)
    let winRate = value["winRate"] as? Float ?? -1
    if winRate == -1 {
      cell.winRateLabel.text = "WAITING FOR JUDGE"
    } else {
      if winRate > 0.5 {
        cell.trophy.isHidden = false
      } else {
        cell.trophy.isHidden = true
      }
      cell.winRateLabel.text = "WIN RATE : " + String((winRate*100).string2)+"%"
    }
    let winCount = value["winCount"] as? Int ?? 0
    cell.likeLabel.text = String(winCount)
    let judgeCount = value["judgeCount"] as? Int ?? 0
    cell.dislikeLabel.text = String(judgeCount-winCount)
    cell.likeIcon.addTarget(self, action: #selector(like), for: .touchUpInside)
    cell.dislikeIcon.addTarget(self, action: #selector(dislike), for: .touchUpInside)
    return cell
  }
  
  func like(){
    
  }
  
  func dislike(){
    
  }
  
  // dismiss gesture
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if(self.tapOutsideRecognizer == nil) {
      self.tapOutsideRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapBehind))
      self.tapOutsideRecognizer.numberOfTapsRequired = 1
      self.tapOutsideRecognizer.cancelsTouchesInView = false
      self.tapOutsideRecognizer.delegate = self
      UIApplication.shared.keyWindow!.addGestureRecognizer(self.tapOutsideRecognizer)
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    if(self.tapOutsideRecognizer != nil) {
      UIApplication.shared.keyWindow!.removeGestureRecognizer(self.tapOutsideRecognizer)
      self.tapOutsideRecognizer = nil
    }
  }
  
  func handleTapBehind(sender: UITapGestureRecognizer) {
    if (sender.state == UIGestureRecognizerState.ended) {
      let location: CGPoint = sender.location(in: nil)
      
      if (!self.view.point(inside: self.view.convert(location, from: self.view.window), with: nil)) {
        self.view.window?.removeGestureRecognizer(sender)
        self.dismiss(animated: true, completion: nil)
      }
    }
  }
  
  
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    return true
  }
}
