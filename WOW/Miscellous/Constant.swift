//
//  Constant.swift
//  MusicBook
//
//  Created by SiuEdwin on 7/5/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//


// film: http://www.flaticon.com/free-icon/film-strip-with-two-photograms_25311#term=film&page=1&position=32
// calendar: flat search calendar music
// profile: iconsdb user.png
// brown: 561A04 86 26 4
// grey: E94426 128 128 128

import UIKit
// MARK: COLOR
let COLOR_BLUE = UIColor.init(red: 20/255.0, green: 120/255.0, blue: 240/255.0, alpha: 1.0)
let COLOR_BLUE_LOW_ALPHA = UIColor.init(red: 20/255.0, green: 120/255.0, blue: 240/255.0, alpha: 0.5)
let COLOR_RED = UIColor.init(red: 234/255.0, green: 67/255.0, blue: 53/255.0, alpha: 1.0)
let COLOR_RED_LOW_ALPHA = UIColor.init(red: 234/255.0, green: 67/255.0, blue: 53/255.0, alpha: 0.5)

// MARK: API
let API_BASE_URL = "xxx.com/user"
let API_VENUE = "venue" //xxx.com/venues
let API_VENUE_DETAIL = "%@/%@" //xxx.com/venue/{vid}/{email}
let API_BOOK = "%@/%@/%@/%@"  //xxx.com/{vid}/{R1}/{datetime, datetime}/{email}
let API_HISTORY = "%@" //xxx.com/{email}
let API_GET = "GET"
let API_DELETE = "DELETE"
let API_POST = "POST"
let API_PUT = "PUT"
let API_UPDATE = "UPDATE"


// MARK: font
let FONT = "DINOT"
let FONT_ITALIC = "DINOT-ITALIC"

// MARK: device
let DEVICE_BOUNDS = UIScreen.main.bounds
let DEVICE_WIDTH = DEVICE_BOUNDS.size.width
let DEVICE_HEIGHT = DEVICE_BOUNDS.size.height


// MARK: 
let NAV_HEIGHT = CGFloat(44.0)
let STATUS_BAR_HEIGHT = UIApplication.shared.statusBarFrame.size.height
class Constant: Any {
}
