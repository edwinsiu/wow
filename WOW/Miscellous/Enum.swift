//
//  Constant.swift
//  MusicBook
//
//  Created by SiuEdwin on 7/5/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//


// film: http://www.flaticon.com/free-icon/film-strip-with-two-photograms_25311#term=film&page=1&position=32
// calendar: flat search calendar music
// profile: iconsdb user.png
// brown: 561A04 86 26 4
// grey: E94426 128 128 128

import UIKit

// MARK: interface
enum UIUserInterfaceIdiom : Int {
  case Unspecified
  case Phone // iPhone and iPod touch style UI
  case Pad // iPad style UI
}

enum Winner : Int {
  case red // iPad style UI
  case blue // iPhone and iPod touch style UI
  case draw
  case not_assigned
}

class Enum: Any {

}
