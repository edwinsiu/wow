//
//  Constant.swift
//  MusicBook
//
//  Created by SiuEdwin on 7/5/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//



import UIKit

extension UIView {
  func addBackground(_ backgroundColor:UIColor)-> UIView{
    let target = self.commonAddBackground(backgroundColor)
    target.addAutolayoutConstraintsSameFrameWithSuperView()
    target.clipsToBounds = true
    return target
  }
  
  func addBackground(_ backgroundColor:UIColor,margin:CGFloat)-> UIView{
    let target = self.commonAddBackground(backgroundColor)
    target.addAutolayoutConstraints("V:|-margin-[view]-margin-|", horizontal: "H:|-margin-[view]-margin-|",variables: ["margin":margin])
    return target
  }
  
  func commonAddBackground(_ backgroundColor:UIColor)-> UIView{
    let testFrame : CGRect = frame
    let testView : UIView = UIView(frame: testFrame)
    testView.backgroundColor = backgroundColor
    self.addSubview(testView)
    self.sendSubview(toBack: testView)
    return testView
  }
  
  func addCenterAutolayoutConstraints(_ width:CGFloat, height: CGFloat, xShift: CGFloat, yShift: CGFloat) {
    self.addCenterAutolayoutConstraints(width, height: height, xShift: xShift, yShift: yShift, target: self.superview!)
  }
  
  func addCenterAutolayoutConstraints(_ width:CGFloat, height: CGFloat, xShift: CGFloat, yShift: CGFloat, target:UIView) {
    self.translatesAutoresizingMaskIntoConstraints = false
    let widthConstraint = NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal,
                                             toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: width)
    let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal,
                                              toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: height)
    
    let xConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: target, attribute: .centerX, multiplier: 1, constant: xShift)
    
    let yConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: target, attribute: .centerY, multiplier: 1, constant: yShift)
    NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
  }
  
  
  func addAutolayoutConstraintsSameFrameWithSuperView(){
    self.addAutolayoutConstraints("V:|-0-[view]-0-|", horizontal: "H:|-0-[view]-0-|")
  }
  
  func addAutolayoutConstraints(_ vertical:String, horizontal:String) -> Void{
    self.addAutolayoutConstraints(vertical, horizontal: horizontal,variables: ["":""])
  }
  
  func addAutolayoutConstraints(_ vertical:String, horizontal:String, variables:NSDictionary) -> Void{
    self.translatesAutoresizingMaskIntoConstraints = false
    let views = ["view" : self]
    let formatString = vertical
    let constraints = NSLayoutConstraint.constraints(withVisualFormat: formatString, options:[] , metrics: variables as? [String : Any], views: views)
    NSLayoutConstraint.activate(constraints)
    self.superview?.addConstraints(constraints)
    
    let formatString2 = horizontal
    let constraints2 = NSLayoutConstraint.constraints(withVisualFormat: formatString2, options:[] , metrics: variables as? [String : Any], views: views)
    NSLayoutConstraint.activate(constraints2)
    self.superview?.addConstraints(constraints2)
  }
  
  func addSubviewAutolayoutConstraints(_ common:String, link:String, views:[String:UIView]) -> Void{
    let constraints = NSLayoutConstraint.constraints(withVisualFormat: link, options:[] , metrics: nil, views: views)
    NSLayoutConstraint.activate(constraints)
    self.addConstraints(constraints)
    for label in views.values {
      label.translatesAutoresizingMaskIntoConstraints = false
      let formatString2 = common
      let constraints2 = NSLayoutConstraint.constraints(withVisualFormat: formatString2, options:[] , metrics: nil, views: ["view":label])
      NSLayoutConstraint.activate(constraints2)
      self.addConstraints(constraints2)
    }
  }
  
  func appearWithAnimation(_ block:@escaping ()->Void){
    self.alpha = 0
    UIView.animate(withDuration: 0.3, animations: {()->Void in
      self.alpha = 1
    }, completion:{(finished:Bool)->Void in
      block()
    })
  }
  
  func disappearWithAnimation(_ block:@escaping ()->Void){
    self.alpha = 1
    UIView.animate(withDuration: 0.3, animations: {()->Void in
      self.alpha = 0
    }, completion:{(finished:Bool)->Void in
      block()
    })
  }
  
  func makeCircle(){
    self.clipsToBounds = true
    self.layer.cornerRadius = self.frame.size.width/2
  }
  
  func makeCircle(_ width:CGFloat){
    self.clipsToBounds = true
    self.layer.cornerRadius = width/2
  }
  
  func addBotLine(){
    self.addLine(0,isTop: false)
  }
  
  func addTopLine(){
    self.addLine(0,isTop: true)
  }
  
  func addLine(_ margin:CGFloat,isTop:Bool){
    let line = UIView()
    line.backgroundColor = UIColor.lightGray
    line.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(line)
    if isTop {
      line.addAutolayoutConstraints("V:|-0-[view(1)]", horizontal: "H:|-0-[view]-0-|",variables: ["margin":margin])
    } else {
      line.addAutolayoutConstraints("V:[view(1)]-0-|", horizontal: "H:|-margin-[view]-margin-|",variables: ["margin":margin])
    }
  }
}

extension String {
  
  func widthOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSFontAttributeName: font]
    let size = self.size(attributes: fontAttributes)
    return size.width
  }
  
  func heightOfString(usingFont font: UIFont) -> CGFloat {
    let fontAttributes = [NSFontAttributeName: font]
    let size = self.size(attributes: fontAttributes)
    return size.height
  }
}

extension UITableView {
  func addBackgroundViewWithLabel(text:String!){
    let view = UIView.init(frame: self.frame)
    let label = UILabel.init()
    view.addSubview(label)
    label.addCenterAutolayoutConstraints(DEVICE_WIDTH, height: 100, xShift: 0, yShift: 0)
    label.text = text
    label.textAlignment = NSTextAlignment.center
    label.textColor = UIColor.black
    self.backgroundView = view
  }
  
  func addBackgroundViewWithLoading(){
    let view2 = UIView.init(frame: self.frame)
    let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray )
    activityIndicator.isHidden = false
    activityIndicator.alpha = 1.0;
    activityIndicator.startAnimating()
    view2.addSubview(activityIndicator)
    activityIndicator.center = view2.center
    self.backgroundView = view2
  }
}

extension Dictionary {
  mutating func merge(with dictionary: Dictionary) {
    dictionary.forEach { updateValue($1, forKey: $0) }
  }
  func merged(with dictionary: Dictionary) -> Dictionary {
    var dict = self
    dict.merge(with: dictionary)
    return dict
  }
}

extension Float {
  var string1: String {
    return String(format: "%.1f", self)
  }
  var string2: String {
    return String(format: "%.2f", self)
  }
}
