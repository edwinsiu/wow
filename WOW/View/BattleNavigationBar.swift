//
//  BattleNavigationBar.swift
//  WOW
//
//  Created by SiuEdwin on 18/12/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit

class BattleNavigationBar: UINavigationBar{
  var progressView : UIProgressView! = UIProgressView()
  var redAvatar : UIImageView! = UIImageView()
  var blueAvatar : UIImageView! = UIImageView()
  var redLabel : UILabel! = UILabel()
  var blueLabel : UILabel! = UILabel()
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.addSubview(progressView)
    self.backgroundColor = UIColor.clear
    progressView.progressViewStyle = .bar
    progressView.progressTintColor = COLOR_RED
    progressView.trackTintColor = COLOR_BLUE
    
    self.addSubview(redAvatar)
    redAvatar.backgroundColor = COLOR_RED
    redAvatar.layer.borderWidth = 1.0
    redAvatar.layer.borderColor = COLOR_RED.cgColor
    redAvatar.clipsToBounds = true
    redAvatar.contentMode = UIViewContentMode.scaleAspectFit
    
    self.addSubview(blueAvatar)
    blueAvatar.backgroundColor = COLOR_BLUE
    blueAvatar.layer.borderWidth = 1.0
    blueAvatar.layer.borderColor = COLOR_BLUE.cgColor
    blueAvatar.clipsToBounds = true
    blueAvatar.contentMode = UIViewContentMode.scaleAspectFit
    
    redLabel.numberOfLines = 0
    redLabel.textColor = .white
    redLabel.textAlignment = .left
    redLabel.font = UIFont(name: FONT, size: 13)
    self.addSubview(redLabel)
    
    blueLabel.numberOfLines = 0
    blueLabel.textColor = .white
    blueLabel.textAlignment = .right
    blueLabel.font = UIFont(name: FONT, size: 13)
    self.addSubview(blueLabel)
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func updateConstraints() {
    let margin = 10
    let marginH = Int(self.frame.height)-margin
    let progressMargin = UIDevice.current.userInterfaceIdiom == .pad ? 30 : 20
    progressView.addAutolayoutConstraints("V:|-progressMargin-[view]-progressMargin-|", horizontal: "H:|-marginH-[view]-marginH-|",variables:["marginH":marginH,"margin":margin,"progressMargin":progressMargin])
    redAvatar.addAutolayoutConstraints("V:|-margin-[view]-margin-|", horizontal: "H:|-margin-[view(height)]",variables:["height":Int(self.frame.height)-margin*2,"margin":margin])
    blueAvatar.addAutolayoutConstraints("V:|-margin-[view]-margin-|", horizontal: "H:[view(height)]-10-|",variables:["height":Int(self.frame.height)-margin*2,"margin":margin])
    redLabel.addAutolayoutConstraints("V:|-margin-[view]-margin-|", horizontal: "H:|-marginH-[view(130)]", variables: ["marginH":marginH+5,"margin":margin])
    blueLabel.addAutolayoutConstraints("V:|-margin-[view]-margin-|", horizontal: "H:[view(130)]-marginH-|", variables: ["marginH":marginH+5,"margin":margin])
    super.updateConstraints()
  }
}
