//
//  HistoryTableViewCell.swift
//  MusicBook
//
//  Created by User on 24/11/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit

class BattleTableViewCell: UITableViewCell {
  var titleLabel : UILabel! = UILabel()
  var backgroundImage : UIImageView! = UIImageView()
  var gradient = CAGradientLayer()
  var trophy = UIImageView()
  private let margin = CGFloat(15.0)
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundImage.backgroundColor = UIColor.lightGray
    backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
    backgroundImage.clipsToBounds = true
    self.contentView.addSubview(backgroundImage)
    
    titleLabel.font = UIFont(name: FONT, size: 21)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .white
    self.contentView.addSubview(titleLabel)

    gradient = CAGradientLayer()
    gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
//    backgroundImage.layer.insertSublayer(gradient, at: 0)
    
    self.addSubview(trophy)
    trophy.image = UIImage.init(named: "trophy.png")
    self.setNeedsUpdateConstraints()
  }
  
  override func layoutSublayers(of layer: CALayer) {
    super.layoutSublayers(of: layer)
    gradient.frame = CGRect(x: 0, y: self.frame.height - 30, width: DEVICE_WIDTH, height: 30)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func updateConstraints() {
    titleLabel.addAutolayoutConstraints("V:|-margin-[view(30)]",horizontal: "H:|-margin-[view]-70-|", variables: ["margin":margin])
    backgroundImage.addAutolayoutConstraintsSameFrameWithSuperView()
    super.updateConstraints()
  }
  
  func updateTitleLabelWithAttributedText(text:NSAttributedString){
    let trophySize = CGFloat(20.0)
    titleLabel.attributedText = text
    let width = text.string.widthOfString(usingFont: titleLabel.font)
    trophy.frame = CGRect(x: width - 10, y: 20, width: trophySize, height: trophySize)
  }
}
