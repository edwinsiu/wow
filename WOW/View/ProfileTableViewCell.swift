//
//  HistoryTableViewCell.swift
//  MusicBook
//
//  Created by User on 24/11/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
  //  var chatButton : UIButton!
  var titleLabel = UILabel()
  var trophy = UIImageView()
  var backgroundImage : UIImageView! = UIImageView()
  var botView = UIView()
  var nameLabel = UILabel()
  var winRateLabel = UILabel()
  var likeIcon = UIButton()
  var likeLabel = UILabel()
  var dislikeIcon = UIButton()
  var dislikeLabel = UILabel()
  private let margin = CGFloat(15.0)
  private let titleHeight = CGFloat(20.0)
  private let botViewHeight = CGFloat(50.0)
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    backgroundImage.backgroundColor = UIColor.lightGray
    backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
    backgroundImage.clipsToBounds = true
    self.contentView.addSubview(backgroundImage)
    
    titleLabel.font = UIFont(name: FONT, size: 16)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .white
    titleLabel.textAlignment = .center
    titleLabel.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.6)
    self.contentView.addSubview(titleLabel)
    
    nameLabel.font = UIFont(name: FONT, size: 16)
    nameLabel.numberOfLines = 0
    nameLabel.textColor = .black
    nameLabel.textAlignment = .left
    botView.addSubview(nameLabel)
    
    likeLabel.font = UIFont(name: FONT, size: 10)
    likeLabel.numberOfLines = 0
    likeLabel.textColor = .black
    likeLabel.textAlignment = .center
    botView.addSubview(likeLabel)
    
    dislikeLabel.font = UIFont(name: FONT, size: 10)
    dislikeLabel.numberOfLines = 0
    dislikeLabel.textColor = .black
    dislikeLabel.textAlignment = .center
    botView.addSubview(dislikeLabel)
    
    
    winRateLabel.font = UIFont(name: FONT, size: 14)
    winRateLabel.numberOfLines = 0
    winRateLabel.textColor = .red
    winRateLabel.textAlignment = .left
    
    self.contentView.addSubview(botView)
    botView.addBotLine()
    botView.addSubview(winRateLabel)
    
    botView.addSubview(trophy)
    trophy.image = UIImage.init(named: "trophy.png")
    
    botView.addSubview(likeIcon)
    likeIcon.setImage(UIImage.init(named: "like.png"),for:.normal)
    
    botView.addSubview(dislikeIcon)
    dislikeIcon.setImage(UIImage.init(named: "dislike.png"),for:.normal)
    self.setNeedsUpdateConstraints()
  }
  
  override func layoutSublayers(of layer: CALayer) {
    super.layoutSublayers(of: layer)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func updateConstraints() {
    let backgroundImageHeight = self.frame.width/2
    let margin = CGFloat(15)
    backgroundImage.addAutolayoutConstraints("V:|-0-[view(height)]", horizontal: "H:|-0-[view]-0-|",variables:["height":backgroundImageHeight])
    titleLabel.addAutolayoutConstraints("V:|-0-[view(height)]", horizontal: "H:|-0-[view]-0-|",variables:["height":titleHeight])
    botView.addAutolayoutConstraints("V:|-margin-[view(height)]", horizontal: "H:|-0-[view]-0-|",variables:["margin":backgroundImageHeight,"height":botViewHeight])
    nameLabel.addAutolayoutConstraints("V:|-0-[view(height)]", horizontal: "H:|-margin-[view]-0-|",variables:["height":botViewHeight/2,"margin":margin])
    winRateLabel.addAutolayoutConstraints("V:[view(height)]-0-|", horizontal: "H:|-margin-[view]-0-|",variables:["height":botViewHeight/2,"margin":margin])
    dislikeIcon.addAutolayoutConstraints("V:|-5-[view(30)]", horizontal: "H:[view(30)]-10-|")
    likeIcon.addAutolayoutConstraints("V:|-5-[view(30)]", horizontal: "H:[view(30)]-50-|")
    
    dislikeLabel.addAutolayoutConstraints("V:[view(10)]-5-|", horizontal: "H:[view(30)]-10-|")
    likeLabel.addAutolayoutConstraints("V:[view(10)]-5-|", horizontal: "H:[view(30)]-50-|")
    super.updateConstraints()
  }
  
  
  func updateTitleLabelWithText(text:String){
    let trophySize = CGFloat(18.0)
    nameLabel.text = text
    let width = text.widthOfString(usingFont: titleLabel.font)
    trophy.frame = CGRect(x: width + 18, y: 5, width: trophySize, height: trophySize)
  }
}
