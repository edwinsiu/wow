//
//  ProfileHeaderView.swift
//  WOW
//
//  Created by User on 20/12/2016.
//  Copyright © 2016 MusicBook. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {
  var avatar = UIImageView()
  var titleLabel = UILabel()
  var backgroundImage : UIImageView! = UIImageView()
  var blurView : UIVisualEffectView! = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
  override init(frame: CGRect) {
    super.init(frame: frame)
    backgroundImage.backgroundColor = UIColor.lightGray
    backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
    backgroundImage.clipsToBounds = true
    self.addSubview(backgroundImage)
    
    self.addSubview(blurView)
    blurView.alpha = 0.8
    blurView.clipsToBounds = true
    avatar.contentMode = UIViewContentMode.scaleAspectFill
    avatar.clipsToBounds = true
    avatar.backgroundColor = UIColor.lightGray
    self.addSubview(avatar)
    
    titleLabel.font = UIFont(name: FONT, size: 13)
    titleLabel.numberOfLines = 0
    titleLabel.textColor = .white
    titleLabel.textAlignment = .center
    self.addSubview(titleLabel)
    self.setNeedsUpdateConstraints()
  }
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    avatar.makeCircle()
  }
  
  override func updateConstraints() {
    let height = CGFloat(30.0)
    let margin = CGFloat(10.0)
    backgroundImage.addAutolayoutConstraintsSameFrameWithSuperView()
    blurView.addAutolayoutConstraintsSameFrameWithSuperView()
    avatar.addCenterAutolayoutConstraints(DEVICE_WIDTH/2 - height, height: DEVICE_WIDTH/2 - height, xShift: 0, yShift: -1*margin)
    titleLabel.addAutolayoutConstraints("V:[view(height)]-margin-|", horizontal: "H:|-0-[view]-0-|",variables:["margin":margin,"height":height])
    super.updateConstraints()
  }
}
